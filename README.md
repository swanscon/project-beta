# CarCar

Team:

- Connor - Services microservice
- Andres - Sales microservice

## Design

- Bootstrap v5 is used throughout the Web Application.

## Services microservice

#### AutomobileVO model:

- import_href (CharField)
- vin (CharField)

#### Technician model:

- name (CharField)
- employee_number (PositiveSmallIntegerField)

#### Service model:

- customer (CharField)
- date_time (DateTimeField)
- reason (CharField)
- finished (BooleanField)
- technician (ForeignKey of Technician)
- vin (ForeignKey of AutomobileVO)

### /technician/new: fetches from the technician list API.

- the employee_number must be unique.
- sends a 'POST' request to the technician API.

### /services/new: fetches from the services list API.

- the VIN must match the VIN of an Automobile that exists.
- the date_time property is split on the form to allow DATE and TIME to be entered independently although stored in the same place in the API.
- a technician must exist to be selected when creating a service.
  sends a 'POST' request to the services API.

### /services/appointments: fetches from the services list API.

- sortTable and sortTableTime functions are imported to sort the list ascending by Date, and the Times if the dates are the same.
- serviceDate and serviceTime functions are imported to convert the 'date_time' property of the services into more readable dates and times.
- each row has an option to Cancel or Finish
- The button to Cancel opens a confirmation page linked to the ID of the service on that row by fetching from the detail view API at that ID. Cancelling from that page sends a 'DELETE' request to the API for that service.
- The button to Finish sets the state of the service's finished property on that row to 'true' (default is 'false' when created). This removes it from the list as it only renders services where its state of finished is 'true', but the item remains in the API as this is a 'PUT' request.

### /services/search: fetches from the services list API.

- serviceDate function is imported to convert the 'date_time' property into a readable date.
- handleFinished function is imported to change the 'finished' property boolean value 'true' to a checkmark and 'false' to an empty string.
- handleFilter function sets the state to match the 'vin' or 'customer name' typed into the search bar causing only those services to display.
- sortTable function uses for loops and if statements to allow for toggling how the table is sorted depending on column (ascending or descending). The function also adds an arrow to indicate whether the rows are ascending or descending based on the row clicked.

## Sales microservice

Andres Galvez Carrillo is going to do Sales
Explain your models and integration with the inventory
microservice, here.

#### AutomobileVO model:

-import_href (CharField)
-vin (CharField)
-color (CharField)
-year (PositiveSmallIntegerField)
-model (CharField)
-manufactures (CharField)

####  SalesEmployee model:

- name (CharField)
- employee_num (PositiveSmallIntegerField)

####  Customer model:

- name (CharField)
- address (CharField)
-phone (CharField)
#### Service model:


-  salePirce  (IntegerField)
- customer sold  (ForeignKey of customer )
- employee sold  (ForeignKey of Sale employee)
- vin (ForeignKey of AutomobileVO)