from django.db import models
from django.urls import reverse

# Create your models here.
class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True)
    vin = models.CharField(max_length=50, unique=True)

class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.PositiveSmallIntegerField(unique=True)

class Service(models.Model):
    customer_name = models.CharField(max_length=100)
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=100)
    finished = models.BooleanField(default=False)
    vin = models.ForeignKey(
        AutomobileVO,
        related_name="services",
        on_delete=models.CASCADE,
    )
    technician = models.ForeignKey(
        Technician,
        related_name="services",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.customer_name

    def get_api_url(self):
        return reverse('api_service', kwargs={'pk': self.pk})
