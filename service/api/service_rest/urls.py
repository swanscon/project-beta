from django.urls import path
from .views import api_list_services, api_service, api_list_technicians, api_technician

urlpatterns = [
    path("services/", api_list_services, name="api_list_services"),
    path("services/<int:pk>", api_service, name="api_service"),
    path("technicians/", api_list_technicians, name="api_list_technicians"),
    path("technicians/<int:pk>", api_technician, name="api_technician"),
]
