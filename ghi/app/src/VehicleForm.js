import React from "react";

class VehicleForm extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            name: '',
            manufacturer: '',
            manufacturers: [],
            pictureUrl: '',
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async componentDidMount() {
        try {
            const url = 'http://localhost:8100/api/manufacturers/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                this.setState({ manufacturers: data.manufacturers })
            } else {
                throw (new Error("Response Error", response));
            }
        } catch (err) {
            console.log(err);
    }}
    handleChange(event) {
        const newState = {};
        newState[event.target.id] = event.target.value;
        this.setState(newState);
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.picture_url = data.pictureUrl;
        data.manufacturer_id = data.manufacturer;
        delete data.pictureUrl;
        delete data.manufacturer;
        delete data.manufacturers;
        const submitUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };
        console.log(data);
        const response = await fetch(submitUrl, fetchConfig);
        if (response.ok) {
            const newVehicle = await response.json();
            const cleared = {
                name: '',
                manufacturer: '',
                manufacturers: [],
                pictureUrl: '',
            };
            this.setState(cleared);
        }
    }

    render() {
        return(
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a vehicle model</h1>
                        <form onSubmit={this.handleSubmit} id="create-vehicle-form">
                            <div className="form-group row mb-3">
                                <input onChange={this.handleChange} value={this.state.name} placeholder="Model name" required type="text" name="name" id="name"/>
                            </div>
                            <div className="form-group row mb-3">
                                <select onChange={this.handleChange} value={this.state.manufacturer} required id="manufacturer" name="manufacturer">
                                    <option>Select a manufacturer</option>
                                    {this.state.manufacturers.map(manufacturer => {
                                        return (
                                            <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-group row mb-3">
                                <input onChange={this.handleChange} value={this.state.pictureUrl} placeholder="Picture URL" required type="text" name="pictureUrl" id="pictureUrl"/>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default VehicleForm;