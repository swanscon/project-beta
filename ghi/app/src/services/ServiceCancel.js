import React from "react";
import { Navigate, NavLink } from 'react-router-dom';

function serviceDate(dateTime) {
    const date = new Date(dateTime)
    const month = date.getUTCMonth()+1;
    const day = date.getUTCDate();
    const year = date.getFullYear();
    return (
        <div>
            {month}/{day}/{year}
        </div>
    )
}

function serviceTime(dateTime) {
    const time = new Date(dateTime)
    const hour = time.getUTCHours();
    let setHour = hour
    if (hour === 0) {
        setHour = 12
    } else if (hour > 12) {
        setHour -= 12
    }
    let minute = time.getMinutes();
    if (minute < 10) {
        minute = '0'+minute
    }
    if (hour < 12) {
        return (
            <div>
                {setHour}:{minute} AM
            </div>
        )
    }
    return (
        <div>
            {setHour}:{minute} PM
        </div>
    )
}

class ServiceCancel extends React.Component {
        constructor(props) {
            super(props);
            this.state={
                id: window.location.href.slice(38),
                redirect: false,
                customerName: '',
                dateTime: '',
                reason: '',
                vin: '',
                technician: '',
            }
            this.cancelService = this.cancelService.bind(this)
        }
        async componentDidMount() {
            const url = `http://localhost:8080/api/services/${this.state.id}`
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                this.setState({
                    customerName: data.customer_name,
                    dateTime: data.date_time,
                    reason: data.reason,
                    vin: data.vin.vin,
                    technician: data.technician.name,
                })
            }
        }

        async cancelService() {
            await fetch(`http://localhost:8080/api/services/${this.state.id}` ,{
                method: 'delete'
            }).then((result)=>{
                result.json()
            });
            this.setState({
                redirect: true,
            })
        }

        renderRedirect = () => {
            if (this.state.redirect) {
                return <Navigate to='/services/appointments' />
            }
        }

        render() {
            return(
            <div>
                {this.renderRedirect()}
                <h1>Confirm cancellation</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Customer name</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{this.state.vin}</td>
                            <td>{this.state.customerName}</td>
                            <td>{serviceDate(this.state.dateTime)}</td>
                            <td>{serviceTime(this.state.dateTime)}</td>
                            <td>{this.state.technician}</td>
                            <td>{this.state.reason}</td>
                        </tr>
                    </tbody>
                </table>
                <div className="text-center">
                    <h6>Are you sure?</h6>
                    <button type="button" className="btn btn-danger" onClick={this.cancelService}>
                        Cancel
                    </button>
                    <br></br><br></br>
                    <div>
                        <NavLink className="text-muted" to="/services/appointments"><u>Return to appointments list</u></NavLink>
                    </div>
                </div>
            </div>
        )
    }
}

export default ServiceCancel;