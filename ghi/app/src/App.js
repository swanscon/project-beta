import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import VehicleList from './VehicleList';
import VehicleForm from './VehicleForm';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import SaleMainPage from './sales/SaleMainPage';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import ServiceList from './services/ServiceList';
import CustomerForm from './sales/Customer';
import EmployeeForm from './sales/Employee';
import SaleForm from './sales/SaleForm';
import TechnicianForm from './services/TechnicianForm';
import ServiceForm from './services/ServiceForm';
import ServiceSearch from './services/ServiceSearch';
import SaleList from './sales/SaleList';
import ServiceCancel from './services/ServiceCancel';
function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="vehicles" element={<VehicleList />}/>
          <Route path="vehicles/new" element={<VehicleForm />}/>
          <Route path="manufacturers" element={<ManufacturerList />}/>
          <Route path="manufacturers/new" element={<ManufacturerForm />}></Route>
          <Route path="automobiles" element={<AutomobileList />}/>
          <Route path="automobiles/new" element={<AutomobileForm />}/>
          <Route path="services/appointments" element={<ServiceList />}/>
          <Route path="sale" element={<SaleMainPage />} />
          <Route path="sale/form" element={<SaleForm />} />
          
          <Route path="sale/customer" element={<CustomerForm />} />
          <Route path="sale/employee" element={<EmployeeForm />} />
          <Route path="sale/sold" element={<SaleList />} />
          
          <Route path="technician/new" element={<TechnicianForm />}/>
          <Route path="services/new" element={<ServiceForm />}/>
          <Route path="services/search" element={<ServiceSearch />}/>
          <Route path="services/cancel/:serviceId" element={<ServiceCancel />}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
