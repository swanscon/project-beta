import React from 'react';
class CustomerForm extends React.Component {
    constructor(props){
      super(props)
      this.state = {
        name:'',
        address:'',
        phone:''
       
      };
  
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this);
      }
      async handleSubmit(event) {
        event.preventDefault();
        const data = {
          name:this.state.name,
          address:this.state.address,
          phone:this.state.phone,
      };
        
          const CustomerUrl = 'http://localhost:8090/api/customer/';
          const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
          };
          const response = await fetch(CustomerUrl, fetchConfig);
          if (response.ok) {
            const newCustomer = await response.json();
            console.log(newCustomer);
            const cleared = {
              name: '',
              address:'',
              phone:''
              
            };
            this.setState(cleared);
          }
        }
        handleChange(event) {
          const newState = {};
          newState[event.target.id] = event.target.value;
          this.setState(newState);
          console.log(newState)
        }
    
     
     
      render() {
        return (
          <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Potential Customer Info</h1>
                <form onSubmit={this.handleSubmit} id="create-location-form">
                  <div className="form-floating mb-3">
                  <input onChange={this.handleChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={this.state.name} />
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                  <input onChange={this.handleChange} placeholder="Address" required type="text" name="address" id="address" className="form-control" value={this.state.address} />
                    <label htmlFor="address">address</label>
                  </div>
                  <div className="form-floating mb-3">
                  <input onChange={this.handleChange} placeholder="phone" required type="text" name="phone" id="phone" maxLength="10" size="10" className="form-control" value={this.state.phone} />
                    <label htmlFor="phone">phone</label>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
        );
      }
    }
  export default CustomerForm;