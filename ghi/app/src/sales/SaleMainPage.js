import {NavLink, Outlet} from 'react-router-dom'
import React from 'react';

class SaleMainPage extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      vin:'',
      make:'',
      model:'',
      filter:[],
      filterModel:[],
      manufacturer:[],
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleFilterClick=this.handleFilterClick.bind(this);
    this.ModelSelect=this.ModelSelect.bind(this);
    this.handleFilterModelClick=this.handleFilterModelClick.bind(this);
    this.handleFilterCarClick=this.handleFilterCarClick.bind(this)
    
   
  }
  ModelSelect(prop){
    if(prop===''){
    return(
      <div  className="card text-center">
     <div className="card-header">
       <ul className="nav nav-tabs card-header-tabs">
         <li className="nav-item">
           <a className="nav-link active" aria-current="true" href="">Make</a>
         </li>
         <li className="nav-item">
           <a className="nav-link" href="">Model</a>
         </li>
         <li className="nav-item">
          <a className="nav-link " href="">Results</a>
        </li>
         </ul>
     </div>
     <div className="card-body">
       <h4 className="card-title">Car Manufacturer</h4>
       <h6 className="card-text">*currently available </h6>
       <p>
       <button className="btn btn-primary"  data-bs-toggle="collapse" onClick={this.handleFilterClick}data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
       select manufactor
       </button>
     </p>
     <div className="collapse" id="collapseExample">
       <div className="card card-body">
         <div>
        <ul >
  
              {this.state.filter.map((item,index)=> {
                
                  return (
                          
                          <li  key={index}><button className="btn btn-primary"  data-bs-toggle="collapse" data-bs-target="#collapseExample" onClick={this.handleChange} id="make" value={item}   type="button">{item}</button>
                      
                          
                          </li>
                       
                  );
              })}
       </ul>
       </div>
       </div>
     </div>
     </div>
     </div>
  );
  ////////This is the SPLIT
  //////
  //////
  }else if(this.state.model===''){
    return(<div  className="card text-center">
    <div className="card-header">
      <ul className="nav nav-tabs card-header-tabs">
        <li className="nav-item">
          <a className="nav-link"  href="">Make</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="">Model</a>
        </li>
        <li className="nav-item">
          <a className="nav-link " href="">Results</a>
        </li>
        </ul>
    </div>
    <div className="card-body">
      <h4 className="card-title">{this.state.make} Models</h4>
      <h6 className="card-text">*currently available </h6>
      <p>
      <button className="btn btn-primary"  data-bs-toggle="collapse" onClick={this.handleFilterModelClick}data-bs-target="#collapseExample"  aria-controls="collapseExample">
      select model
      </button>
    </p>
    <div className="collapse" id="collapseExample">
      <div className="card card-body">
        <div>
       <ul >
 
             {this.state.filterModel.map((item,index)=> {
               
                 return (
                         
                         <li  key={index}><button className="btn btn-primary"   data-bs-toggle="collapse" data-bs-target="#collapseExample" onClick={this.handleChange} id="model" value={item}   >{item}</button>
                         
                         
                         </li>
                      
                 );
             })}
      </ul>
      </div>
      </div>
    </div>
    </div>
    </div>)
  }else{
    return(
      <div  className="card text-center">
    <div className="card-header">
      <ul className="nav nav-tabs card-header-tabs">
        <li className="nav-item">
          <a className="nav-link"  href="">Make</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="">Model</a>
        </li>
        <li className="nav-item">
          <a className="nav-link active" aria-current="true"href="">Results</a>
        </li>
        </ul>
    </div>
    <div className="card-body">
      <h4 className="card-title">{this.state.make} {this.state.model}</h4>
      <h6 className="card-text">currently available </h6>
      <p>
      <button className="btn btn-primary"  data-bs-toggle="collapse" onClick={this.handleFilterCarClick}data-bs-target="#collapseExample"  aria-controls="collapseExample">
      select car 
      </button>
    </p>
    <div className="collapse" id="collapseExample">
      <div className="card card-body">
        <div>
       <ul >
 
             {this.state.filterModel.map((item,index)=> {
              console.log("this is the item ")
              console.log(item)
               
                 return (
                         
                         <li  key={index}><button className="btn btn-primary"   data-bs-toggle="collapse" onClick={this.handleChange} id="vin" value={item.vin}   >{item.color} {item.year} {item.model}</button>
                         
                         
                         </li>
                      
                 );
             })}
      </ul>
      </div>
      </div>
    </div>
    </div>
    </div>
    )
  }
}


 
  
 
  async componentDidMount() {
  
    
    let url = 'http://localhost:8090/api/instock/';
    let response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      this.setState({manufacturer: data.AutomobileVO});
      
    }
     
   }
    handleFilterCarClick(){
      const uniqueModel=[]
      
      this.state.manufacturer.map(item => {
        console.log(this.state.manufacturer)
      
        if(item['model']==this.state.model){
          
          uniqueModel.push(item)
          
          
        }
        
       
    });
    
    this.setState({filterModel: uniqueModel});
    }
    handleFilterModelClick(){
      
      const uniqueModel=[]
      
      this.state.manufacturer.map(item => {
        console.log(this.state.manufacturer)
      
        if(!(uniqueModel.includes(item['model']))&&item['manufacturer']==this.state.make){
          
          uniqueModel.push(item['model'])
          
          
        }
        
       
    });
    
    this.setState({filterModel: uniqueModel});

    }
    
    handleFilterClick(){
       const uniqueManufacturer=[]
        this.state.manufacturer.map(item => {
      
        if(!(uniqueManufacturer.includes(item['manufacturer']))){
          
          uniqueManufacturer.push(item['manufacturer'])
          
          
        }
        
       
    });
    
    this.setState({ filter: uniqueManufacturer});
    
    }
    handleChange(event) {
      const newState = {};
      newState[event.target.id] = event.target.value;
      this.setState(newState);
      console.log(newState)
    }
  render(){
    const modelOption=this.ModelSelect(this.state.make);

    return (
      <div className="px-4 py-5 my-5 text-center">
        <div className="container text-center">
        <div className="row">
 

 
        <div className="col">
          <div>
          <NavLink className="nav-link" to="/sale/customer">
          <button type="button" className="btn btn-primary">New Potential Customer?</button>
          </NavLink>
          </div>
        </div>
        <div className="col-6">
        <div>
          <NavLink className="nav-link" to="/sale/form">
          <button type="button" className="btn btn-primary">Sale Form </button>
          </NavLink>
          </div>
          <div>
          <NavLink className="nav-link" to="/sale/sold">
          <button type="button" className="btn btn-primary">Cars Sold</button>
          </NavLink>
          </div>
      
          <div value={this.state.make}>{modelOption}</div>
        

 </div>
        <div className="col">
          <div>
          <NavLink className="nav-link" to="/sale/employee">
          <button type="button" className="btn btn-primary">Sale Employees </button>
          </NavLink>
          </div>
        </div>
        </div>
</div>
        </div>
      
    );
  }}
  
  export default SaleMainPage;