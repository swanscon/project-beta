
function ModelSelect(){
  return(
    <div  className="card text-center">
   <div className="card-header">
     <ul className="nav nav-tabs card-header-tabs">
       <li className="nav-item">
         <a className="nav-link active" aria-current="true" href="">Make</a>
       </li>
       <li className="nav-item">
         <a className="nav-link" href="">Model</a>
       </li>
       </ul>
   </div>
   <div className="card-body">
     <h4 className="card-title">Car Manufacturer</h4>
     <h6 className="card-text">*currently available </h6>
     <p>
     <button className="btn btn-primary" type="button" data-bs-toggle="collapse" onClick={this.handleFilterClick}data-bs-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
     select manufactor
     </button>
   </p>
   <div className="collapse" id="collapseExample">
     <div className="card card-body">
       <div>
      <ul >

            {this.state.filter.map((item,index)=> {
              
                return (
                        
                        <li  key={index}><button className="btn btn-primary"  data-bs-target={index} data-bs-toggle="collapse" onClick={this.handleChange} id="make" value={item}className="dropdown-item"   type="button">{item}</button>
                          <div className="collapse" id={index}>
                          <div className="card card-body">
                            {this.state.make}
 
                           </div>
                            </div>
                        
                        </li>
                     
                );
            })}
     </ul>
     </div>
     </div>
   </div>
   </div>
   </div>
);
}
export default ModelSelect;

