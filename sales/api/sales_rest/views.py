from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from.models import Customer,AutomobileVO,SalesEmployee,SaleRecord
from .encoders import CustomerEncoder,AutoVoEncoder,EmployeeEncoder,SaleEncoder

# Create your views here.
@require_http_methods(["GET", "POST"])
def api_AutoInStock(request):
    if request.method == "GET":
        filterList= AutomobileVO.objects.filter(sold=False)
        return JsonResponse(
            {"AutomobileVO": filterList},
            encoder=AutoVoEncoder,
        )

@require_http_methods(["GET", "POST"])
def api_AutoVO(request):
    if request.method == "GET":
        AutoVO = AutomobileVO.objects.all()
        return JsonResponse(
            {"AutomobileVO": AutoVO},
            encoder=AutoVoEncoder,
        )
@require_http_methods(["DELETE", "GET", "PUT"])
def api_AutoVOS(request, vin):
    if request.method == "GET":
        try:
            AutoVO = AutomobileVO.objects.get(vin=vin)
            return JsonResponse(
                AutoVO,
                encoder=AutoVoEncoder,
                safe=False
            )
        except AutomobileVO.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            AutoVO = AutomobileVO.objects.get(vin=vin)
            AutoVO.delete()
            return JsonResponse(
                AutoVO,
                encoder=AutoVoEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            AutoVO = AutomobileVO.objects.get(vin = vin)
            props = ["sold"]
            for prop in props:
                if prop in content:
                    setattr(AutoVO, prop, content[prop])
            AutoVO.save()
            
            return JsonResponse(
                AutoVO,
                encoder=AutoVoEncoder,
                safe=False,
            )
        except AutomobileVO.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
        )
        except:
            response = JsonResponse(
                {"message": "Could not create the customer"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_customer(request, pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=pk)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            

            props = ["name"]
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_employees(request):
    if request.method == "GET":
        employee = SalesEmployee.objects.all()
        return JsonResponse(
            {"employee": employee},
            encoder=EmployeeEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            employee = SalesEmployee.objects.create(**content)
            return JsonResponse(
                employee,
                encoder=EmployeeEncoder,
                safe=False,
        )
        except:
            response = JsonResponse(
                {"message": "Could not create the employee"}
            )
            response.status_code = 400
            return response


@require_http_methods(["DELETE", "GET", "PUT"])
def api_employee(request, pk):
    if request.method == "GET":
        try:
            employee = SalesEmployee.objects.get(id=pk)
            return JsonResponse(
                employee,
                encoder=EmployeeEncoder,
                safe=False
            )
        except SalesEmployee.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            employee = SalesEmployee.objects.get(id=pk)
            employee.delete()
            return JsonResponse(
                employee,
                encoder=EmployeeEncoder,
                safe=False,
            )
        except SalesEmployee.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            

            props = ["name"]
            for prop in props:
                if prop in content:
                    setattr(employee, prop, content[prop])
            employee.save()
            return JsonResponse(
                employee,
                encoder=EmployeeEncoder,
                safe=False,
            )
        except SalesEmployee.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response



@require_http_methods(["GET", "POST"])
def api_Sales(request):
    if request.method == "GET":
        Sale = SaleRecord.objects.all()
        return JsonResponse(
            {"Sale": Sale},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            
             
            automobileSold = AutomobileVO.objects.get(import_href= content["automobileSold"])
            content["automobileSold"] = automobileSold
            employee=SalesEmployee.objects.get(employee_num=content["employeeSold"])
            content["employeeSold"]=employee
            customer=Customer.objects.get(name=content["customerSold"])
            content["customerSold"]=customer
            
            print(content)
            Sale = SaleRecord.objects.create(**content)
            response=  JsonResponse(Sale,encoder=SaleEncoder,safe=False,)

        except AutomobileVO.DoesNotExist:
            response= JsonResponse({"message": "Vehicle does not exist"}, status=400)
        return response
      
@require_http_methods(["DELETE", "GET", "PUT"])
def api_sale(request, pk):
    if request.method == "GET":
        try:
            Sale = SaleRecord.objects.get(id=pk)
            return JsonResponse(
                Sale,
                encoder=SaleEncoder,
                safe=False
            )
        except SaleRecord.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            Sale = SaleRecord.objects.get(id=pk)
            Sale.delete()
            return JsonResponse(
                Sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except SaleRecord.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            

            props = ["name"]
            for prop in props:
                if prop in content:
                    setattr(Sale, prop, content[prop])
            Sale.save()
            return JsonResponse(
                Sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except SaleRecord.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

